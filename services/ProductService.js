const base = '/products'
export default {
    getCategories: ($axios) => {
        return $axios.$get(`${base}/categories`)
    },
    getProducts: ($axios) => {
        return $axios.$get(`${base}`)
    },
    getProductsByCategories: ($axios, data) => {
        return $axios.$get(`${base}/category/${data}`)
    }
}