import regex from '@/enums/regularExpressions'

export default {
    documentNumberType: (value) => {
        return value.length == 8 && !!value.match(regex.ONLY_NUMBERS)
    }
}