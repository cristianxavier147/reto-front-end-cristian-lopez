import ProductService from '@/services/ProductService'

export const state = () => ({
    
})

export const getters = {}

export const mutations = {
    
}

export const actions = {
    async getCategories({state, commit}){
        return await ProductService.getCategories(this.$axios)
    },
    async getProducts({state, commit}){
        return await ProductService.getProducts(this.$axios)
    },
    async getProductsByCategories({state, commit}, data){
        return await ProductService.getProductsByCategories(this.$axios, data)
    },
}